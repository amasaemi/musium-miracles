/**
 * gulpfile
 * author: amasaemi <amasaemi@gmail.com>
 * created: 26.12.2017
 */

'use strict';

const gulp = require('gulp'),
  scss = require('gulp-sass'),
  autopref = require('gulp-autoprefixer'),
  wtc = require('gulp-watch'),
  fi = require('gulp-file-include'),
  bs = require('browser-sync');

const path = {
  src: {
    html: 'src/*.html',
    js: 'src/scripts/index.js',
    style: 'src/styles/index.scss',
    assets: 'src/assets/*.*',
    fonts: 'src/fonts/*.otf'
  },
  build: {
    html: 'dist/',
    js: 'dist/js/',
    css: 'dist/css/',
    assets: 'dist/assets/',
    fonts: 'dist/fonts/'
  },
  watch: {
    html: 'src/*.html',
    js: 'src/scripts/**/*.js',
    style: 'src/styles/**/*.scss'
  }
};

// сборка html
gulp.task('html:build', function () {
  gulp.src(path.src.html)
    .pipe(fi())
    .pipe(gulp.dest(path.build.html))
    .pipe(bs.reload({ stream: true }))
});

// сборка js
gulp.task('js:build', function () {
  gulp.src(path.src.js)
    .pipe(gulp.dest(path.build.js))
    .pipe(bs.reload({ stream: true }))
});

// сборка css
gulp.task('style:build', function () {
  gulp.src(path.src.style)
    .pipe(scss())
    .pipe(autopref())
    .pipe(gulp.dest(path.build.css))
    .pipe(bs.reload({ stream: true }))
});

// сборка assets
gulp.task('assets:build', function () {
  gulp.src(path.src.assets)
    .pipe(gulp.dest(path.build.assets))
});

// сборка fonts
gulp.task('fonts:build', function () {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

// сервер
gulp.task('webserver', function () {
  bs({
    server: {
      baseDir: 'dist/'
    },
    host: 'localhost',
    port: 8080,
    tunnel: true
  })
});

// отслеживание изменения в файлах
gulp.task('watcher', function () {
  wtc([path.watch.html], function () {
    gulp.start('html:build');
  });
  wtc([path.watch.js], function () {
    gulp.start('js:build');
  });
  wtc([path.watch.style], function () {
    gulp.start('style:build');
  });
});

// сборка проекта
gulp.task('build', ['html:build', 'js:build', 'style:build', 'assets:build', 'fonts:build']);
// запуск web-сервера
gulp.task('run-server', ['html:build', 'js:build', 'style:build', 'assets:build', 'fonts:build', 'webserver', 'watcher']);

